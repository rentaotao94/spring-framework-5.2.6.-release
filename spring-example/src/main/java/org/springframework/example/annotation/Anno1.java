package org.springframework.example.annotation;

import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author rtt
 * @date 2022/4/13 16:07
 */
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface Anno1 {

	String value() default "";
}
