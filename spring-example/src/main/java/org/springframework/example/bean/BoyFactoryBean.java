package org.springframework.example.bean;

import org.springframework.beans.factory.FactoryBean;

/**
 * @author rtt
 * @date 2022/4/8 11:13
 */
public class BoyFactoryBean implements FactoryBean<Boy> {

	@Override
	public Boy getObject() throws Exception {
		// 可以定制化设置属性
		return new Boy();
	}

	@Override
	public Class<Boy> getObjectType() {
		return Boy.class;
	}
}
