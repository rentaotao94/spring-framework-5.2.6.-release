package org.springframework.example.component;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.*;
import org.springframework.stereotype.Component;

/**
 * @author rtt
 * @date 2022/4/20 14:09
 */
@Configuration
// @ComponentScan()
// @Import("")
// @ImportResource()
public class PeopleConfiguration extends SuperPeopleConfiguration{

	@Bean
	@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
	public Man man() {
		return new Man();
	}
}
