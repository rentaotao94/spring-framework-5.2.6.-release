package org.springframework.example.aop;

import org.springframework.stereotype.Service;

/**
 * @author rtt
 * @date 2022/4/26 10:16
 */
@Service
public class BaseServiceAImpl implements BaseService {

	@Override
	public String work() {
		return "baseServiceAImpl";
	}
}
