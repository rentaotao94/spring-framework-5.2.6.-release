package org.springframework.example.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author rtt
 * @date 2022/4/22 13:41
 */
@Component
public class PeopleFactory {


	@Autowired
	Man man;

	public void get() {
		System.out.println(man.sexy());
	}
}
