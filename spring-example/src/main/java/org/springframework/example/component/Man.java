package org.springframework.example.component;

import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * @author rtt
 * @date 2022/4/15 10:29
 */
public class Man extends People{

	@Autowired
	private Women women;

	public Man() {
		System.out.println("man");
	}

	public String sexy() {
		women.sexy();
		return "man";
	}

	@PostConstruct
	public void postConstruct() {
		System.out.println("man postConstruct");
	}

	@PreDestroy
	public void preDestroy() {
		System.out.println("man preDestroy");
	}

}
