package org.springframework.example.aop;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @author rtt
 * @date 2022/4/26 10:10
 */
public class JdkDynamicProxy  {

	public static void main(String[] args) {
		BaseService serviceA = new BaseServiceAImpl();
		Object proxyInstance = Proxy.newProxyInstance(
				serviceA.getClass().getClassLoader(),
				serviceA.getClass().getInterfaces(),
				(proxy, method, args1) -> {
					System.out.println(proxy.getClass());
					Object result = method.invoke(serviceA, args1);
					System.out.println(result);
					return result;
				}
		);

		BaseService proxyService = (BaseService) proxyInstance;

		System.out.println(proxyService.work());
	}
}
