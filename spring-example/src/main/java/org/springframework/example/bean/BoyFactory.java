package org.springframework.example.bean;

import org.springframework.example.bean.Boy;

/**
 * @author rtt
 * @date 2022/4/11 10:48
 */
public class BoyFactory {

	public static Boy getBoyObject() {
		return new Boy();
	}

	public Boy getObject() {
		return new Boy();
	}
}
