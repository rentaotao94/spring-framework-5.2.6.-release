package org.springframework.example.log;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.spi.ExtendedLogger;
import org.junit.jupiter.api.Test;

/**
 * @author rtt
 */
public class Log4j2Test {

	@Test
	public void test() {
		ExtendedLogger logger = LogManager.getContext().getLogger(Log4j2Test.class);

		logger.error("log4j2 test");
	}
}
