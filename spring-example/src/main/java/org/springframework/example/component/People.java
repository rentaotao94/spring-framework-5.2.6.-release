package org.springframework.example.component;

import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * @author rtt
 * @date 2022/4/13 16:51
 */
@Component
public class People {

	public People() {
		System.out.println("父类构造方法执行");
	}

	@PostConstruct
	public void initMethod() {
		System.out.println("parent initMethod");
	}

	@PreDestroy
	public void  destroyMethod() {
		System.out.println("parent destroyMethod");
	}
}
