package org.springframework.example.bean;

import org.springframework.beans.factory.DisposableBean;

/**
 * @author rtt
 * @date 2022/4/8 09:56
 */
public class Boy implements DisposableBean {

	private Girl girl;

	public Girl getGirl() {
		return girl;
	}

	public void setGirl(Girl girl) {
		this.girl = girl;
	}

	@Override
	public void destroy() throws Exception {
		System.out.println("boy destroy");
	}
}
