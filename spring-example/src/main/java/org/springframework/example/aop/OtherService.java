package org.springframework.example.aop;

import org.springframework.stereotype.Component;

/**
 * @author rtt
 * @date 2022/4/29 16:57
 */
@Component
public class OtherService {

	public String get() {
		return "other";
	}
}
