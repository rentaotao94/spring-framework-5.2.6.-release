package org.springframework.example;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.example.bean.Boy;

/**
 * @author rtt
 * @date 2022/4/13 16:49
 */
public class AnnotationApplicationContextTest {

	public static void main(String[] args) {

		AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();

		applicationContext.register(Boy.class);

		applicationContext.refresh();

		Boy boy = applicationContext.getBean(Boy.class);

		System.out.println(boy);

		applicationContext.close();
	}
}
