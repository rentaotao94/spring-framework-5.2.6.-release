package org.springframework.example.bean;

/**
 * @author rtt
 * @date 2022/4/8 09:56
 */
public class Girl {

	private Boy boy;

	public Boy getBoy() {
		return boy;
	}

	public void setBoy(Boy boy) {
		this.boy = boy;
	}
}
