package org.springframework.example.listen;

import org.springframework.context.ApplicationEvent;

/**
 * @author rtt
 * @date 2022/4/7 14:00
 */
public class MyApplicationEvent extends ApplicationEvent {

	private static final long serialVersionUID = 1L;

	/**
	 * Create a new {@code ApplicationEvent}.
	 *
	 * @param source the object on which the event initially occurred or with
	 *               which the event is associated (never {@code null})
	 */
	public MyApplicationEvent(Object source) {
		super(source);
	}

	public void event() {
		System.out.println("自定义事件逻辑");
	}
}
