package org.springframework.example.component;

import org.springframework.context.annotation.Bean;

/**
 * @author rtt
 * @date 2022/4/20 14:10
 */
public class SuperPeopleConfiguration {

	@Bean
	public Women women() {
		return new Women();
	}
}
