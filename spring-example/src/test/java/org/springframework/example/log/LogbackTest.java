package org.springframework.example.log;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author rtt
 */
public class LogbackTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(LogbackTest.class);

	@Test
	public void test() {

		LOGGER.info("logback test");
	}
}
