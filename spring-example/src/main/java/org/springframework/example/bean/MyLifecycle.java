package org.springframework.example.bean;

import org.springframework.context.Lifecycle;

/**
 * @author rtt
 * @date 2022/4/15 16:25
 */
public class MyLifecycle implements Lifecycle {
	@Override
	public void start() {
		System.out.println("MyLifecycle -> start");
	}

	@Override
	public void stop() {
		System.out.println("MyLifecycle -> stop");
	}

	@Override
	public boolean isRunning() {
		return false;
	}
}
