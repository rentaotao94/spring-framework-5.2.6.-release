package org.springframework.example.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;

/**
 * @author rtt
 * @date 2022/4/26 10:53
 */
@Component
@Aspect
@EnableAspectJAutoProxy
public class DemoAspect {

	@Pointcut("execution(public * org.springframework.example.aop.BaseServiceAImpl.*(..))")
	public void pointcut() {}

	@Before("pointcut()")
	public void before(JoinPoint point) {
		System.out.println("aspect before");
	}

	@After("execution(public * org.springframework.example.aop.OtherService.*(..))")
	public void after(JoinPoint point) {
		System.out.println("aspect after");
	}

	@AfterReturning(pointcut = "pointcut()", returning = "returnVal")
	public void afterReturning(JoinPoint point, Object returnVal) {
		System.out.println("aspect afterReturning");
	}

	@Around("pointcut()")
	public Object around(ProceedingJoinPoint pjp) throws Throwable {

		pjp.proceed();

		return new Object();
	}


	public void test() {}
}
