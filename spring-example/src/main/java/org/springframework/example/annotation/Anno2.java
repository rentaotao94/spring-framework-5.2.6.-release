package org.springframework.example.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author rtt
 * @date 2022/4/13 16:08
 */
@Retention(RetentionPolicy.RUNTIME)
@Anno1
public @interface Anno2{
}
