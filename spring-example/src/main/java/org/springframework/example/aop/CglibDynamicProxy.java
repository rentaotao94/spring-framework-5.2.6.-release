package org.springframework.example.aop;

import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * @author rtt
 * @date 2022/4/26 10:29
 */
public class CglibDynamicProxy {

	public static void main(String[] args) {
		BaseServiceAImpl baseServiceA = new BaseServiceAImpl();

		Enhancer enhancer = new Enhancer();
		enhancer.setSuperclass(BaseServiceAImpl.class);
		enhancer.setCallback((MethodInterceptor) (o, method, objects, methodProxy) -> method.invoke(baseServiceA, objects));
		Object proxyInstance = enhancer.create();

		BaseServiceAImpl serviceA = (BaseServiceAImpl) proxyInstance;

		System.out.println(serviceA.work());
	}
}
