package org.springframework.example.bean;

import org.springframework.context.LifecycleProcessor;

/**
 * @author rtt
 * @date 2022/4/15 16:24
 */
public class MyLifecycleProcessor implements LifecycleProcessor {

	@Override
	public void start() {
		System.out.println("MyLifecycleProcessor -> start");
	}

	@Override
	public void stop() {

	}

	@Override
	public boolean isRunning() {
		return false;
	}

	@Override
	public void onRefresh() {
		System.out.println("MyLifecycleProcessor -> onRefresh");
	}

	@Override
	public void onClose() {

	}
}
