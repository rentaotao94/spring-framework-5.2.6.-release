package org.springframework.example;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.example.bean.BeanExample;

/**
 * @author rtt
 * @date 2022/4/6 15:24
 */
public class MyClassPathXmlApplicationContext extends ClassPathXmlApplicationContext {

	public MyClassPathXmlApplicationContext(String configLocation) {
		super(configLocation);
	}

	@Override
	protected void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) {
		System.out.println("重写 postProcessBeanFactory 方法");

		BeanDefinition definition = beanFactory.getBeanDefinition("beanExample");

		System.out.println(definition.getScope());

		definition.setScope(BeanDefinition.SCOPE_PROTOTYPE);

	}

	public static void main(String[] args) {
		MyClassPathXmlApplicationContext applicationContext = new MyClassPathXmlApplicationContext("application.xml");

		BeanExample beanExample = (BeanExample) applicationContext.getBean("beanExample");

		System.out.println(beanExample.get());
	}
}
