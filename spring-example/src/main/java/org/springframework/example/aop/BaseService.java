package org.springframework.example.aop;

/**
 * @author rtt
 * @date 2022/4/26 10:11
 */
public interface BaseService {

	String work();
}
