package org.springframework.example;

// import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.example.aop.BaseService;
import org.springframework.example.listen.MyApplicationEvent;


/**
 * @author rtt
 * @date 2022/3/31 16:23
 */
@SuppressWarnings("all")
public class BeanFactoryTest {

	public static void main(String[] args) {
		// XmlBeanFactory xmlBeanFactory = new XmlBeanFactory(new ClassPathResource("application.xml"));
		// BeanExample beanExample = (BeanExample) xmlBeanFactory.getBean("beanExample");
		// System.out.println(beanExample.get());

		ClassPathXmlApplicationContext xmlApplicationContext = new ClassPathXmlApplicationContext("application.xml");

		// BeanExample beanExample = (BeanExample) xmlApplicationContext.getBean("beanExample");
		//
		// System.out.println(beanExample.get());

		// Boy boy = (Boy) xmlApplicationContext.getBean("&boyFactory");

		// Object myBeanFactory = xmlApplicationContext.getBean("myBeanFactory");

		// Object bean = xmlApplicationContext.getBean("&boy");
		//
		// System.out.println(bean);

		// source：事件处理器的
		// xmlApplicationContext.publishEvent(new MyApplicationEvent("测试"));

		BaseService baseServiceImpl = (BaseService) xmlApplicationContext.getBean("baseServiceImpl");

		System.out.println(baseServiceImpl.work());

		xmlApplicationContext.close();

	}
}
