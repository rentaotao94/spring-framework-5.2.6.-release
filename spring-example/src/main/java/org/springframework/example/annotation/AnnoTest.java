package org.springframework.example.annotation;

/**
 * @author rtt
 * @date 2022/4/13 16:09
 */
public class AnnoTest {

	/*
		1、接口上的类注解不能被继承
		2、类上的注解如果添加了 @Inherit(只对继承有效果) 注解，能够被继承
		3、注解上的注解不能被直接解析得到
	 */
	public static void main(String[] args) {
		System.out.println(AnnoInter.class.isAnnotationPresent(Anno1.class));
		System.out.println(Anno.class.isAnnotationPresent(Anno1.class));
		System.out.println(AnnoSub.class.isAnnotationPresent(Anno1.class));
		System.out.println(AnnoSub.class.getName());
	}
}
