package org.springframework.example.listen;

import org.springframework.context.ApplicationListener;

/**
 * @author rtt
 * @date 2022/4/7 14:01
 */
public class MyApplicationListener implements ApplicationListener<MyApplicationEvent> {


	@Override
	public void onApplicationEvent(MyApplicationEvent event) {
		event.event();
	}
}
