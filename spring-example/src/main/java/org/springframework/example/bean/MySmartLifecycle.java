package org.springframework.example.bean;

import org.springframework.context.SmartLifecycle;

/**
 * @author rtt
 * @date 2022/4/15 16:34
 */
public class MySmartLifecycle implements SmartLifecycle {
	@Override
	public void start() {
		System.out.println("MySmartLifecycle start");
	}

	@Override
	public void stop() {
		System.out.println("MySmartLifecycle stop");
	}

	@Override
	public boolean isRunning() {
		return false;
	}
}
