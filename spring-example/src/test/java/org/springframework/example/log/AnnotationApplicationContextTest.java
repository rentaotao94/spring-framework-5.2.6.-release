package org.springframework.example.log;

import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.example.bean.Boy;

/**
 * @author rtt
 * @date 2022/4/13 16:49
 */
public class AnnotationApplicationContextTest {

	@Test
	public void test() {

		AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();

		applicationContext.register(Boy.class);

		applicationContext.refresh();

		applicationContext.close();
	}
}
